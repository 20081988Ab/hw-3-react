import { Routes, Route } from "react-router-dom";
import CartRoute from "./routes/CartRoute";
import FavouriteRoute from "./routes/FavouriteRoute";
import ProductsContainer from "./components/ProductsContainer";
import PropTypes from "prop-types";

const AppRouter = ({
  handleAddToFavourite,
  openSecondModalHandler,
  favoriteProducts,
  addedToCartProducts,
  products = [],
  handleDeleteItem 
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <ProductsContainer
            products={products}
            handleAddToFavourite={handleAddToFavourite}
            openSecondModalHandler={openSecondModalHandler}
            favoriteProducts={favoriteProducts}
            handleDeleteItem={handleDeleteItem} // Передача handleDeleteItem далі
          />
        }
      />

      <Route
        path="/cart"
        element={<CartRoute 
            products={addedToCartProducts}
            handleDeleteItem={handleDeleteItem} />}
      />

      <Route
        path="/favourites"
        element={
          <FavouriteRoute
            products={favoriteProducts}
            handleAddToFavourite={handleAddToFavourite}
            openSecondModalHandler={openSecondModalHandler}
            favoriteProducts={favoriteProducts}
          />
        }
      />
    </Routes>
  );
};

AppRouter.propTypes = {
  handleAddToFavourite: PropTypes.func.isRequired,
  openSecondModalHandler: PropTypes.func.isRequired,
  favoriteProducts: PropTypes.array.isRequired,
  addedToCartProducts: PropTypes.array.isRequired,
  products: PropTypes.array,
  handleDeleteItem: PropTypes.func
};

export default AppRouter;
