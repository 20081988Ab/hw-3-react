import styles from "./CartRoute.module.scss";
import PropTypes from "prop-types";
import CartItem from "../../components/CartItem";

const CartRoute = ({
  products = [],
  handleDeleteItem = () => {},
}) => {

  
  return (
    <div className={styles.container}>
      {products.map(product => (
        <CartItem
          key={`${product.id}-${Math.floor(Math.random() * 90) + 1000}`}
          product={product}
          handleDeleteItem={handleDeleteItem} 
        />
      ))}
    </div>
  );
};

CartRoute.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object),
  handleDeleteItem: PropTypes.func
};

export default CartRoute;
