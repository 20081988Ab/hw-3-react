import PropTypes from "prop-types"
import ProductsContainer from "../../components/ProductsContainer"

const FavouriteRoute = ({
    products = [],
    handleAddToFavourite = () => { },
    openSecondModalHandler,
    favoriteProducts = [],

}) => {
    return (
        <>
            <ProductsContainer
                products={products}
                handleAddToFavourite={handleAddToFavourite}
                openSecondModalHandler={openSecondModalHandler}
                favoriteProducts={favoriteProducts}

            />

        </>
    )
}
FavouriteRoute.propTypes = {
    products: PropTypes.array,
    handleAddToFavourite: PropTypes.func,
    openSecondModalHandler: PropTypes.func.isRequired,
    favoriteProducts: PropTypes.array,

}

export default FavouriteRoute ;