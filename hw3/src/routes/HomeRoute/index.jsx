
import PropTypes from 'prop-types';
import ProductsContainer from '../../components/ProductsContainer';

const HomeRoute = ({ products = { products },
    favorite = { favorite },
    handleAddToFavourite = { handleAddToFavourite },
    handleAddToCart = { handleAddToCart },
    addToCart = { addToCart },
    addToCartProduct = () => { },
}) => {

    return (
        <>
            <h1>PRODUCTS</h1>

            <ProductsContainer
                products={products}
                favorite={favorite}
                handleAddToFavourite={handleAddToFavourite}
                handleAddToCart={handleAddToCart}
                addToCart={addToCart}
                addToCartProduct={addToCartProduct}
            />

        </>
    )
}

HomeRoute.propTypes = {
    products: PropTypes.array,
    favorite: PropTypes.array,
    name: PropTypes.string,
    handleAddToFavourite: PropTypes.func,
    handleAddToCart: PropTypes.func,
    addToCart: PropTypes.func,
    addToCartProduct: PropTypes.func
};

export default HomeRoute;
