

import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import { useImmer } from "use-immer";
import ModalWrapper from "./components/Modal/ModalWrapper";
import ProductsContainer from "./components/ProductsContainer";
import Header from "./components/Header";
import "./App.css";
import "./styles.scss";
import AppRouter from "./AppRouter";
import { useLocation } from "react-router-dom";

function App() {
    const URL = "./products.json";
    const { pathname } = useLocation();
    const [productId, setProductId] = useState("");
    const [products, setProducts] = useImmer([]);

    const [chosenProduct, setChosenProduct] = useState({});
    const [addedToCartProducts, setAddedToCartProducts] = useState(() => {
        const savedCartProducts = localStorage.getItem("addedToCartProducts");
        return savedCartProducts ? JSON.parse(savedCartProducts) : [];
    });

    const [favoriteProducts, setFavoriteProducts] = useState(() => {
        const savedFavoriteProducts = localStorage.getItem("favoriteProducts");
        return savedFavoriteProducts ? JSON.parse(savedFavoriteProducts) : [];
    });


    const [isOpenFirstModal, setIsOpenFirstModal] = useState(false);
    const [isOpenSecondModal, setIsOpenSecondModal] = useState(false);

    useEffect(() => {
        const getProducts = async () => {
            try {
                const { data } = await axios.get(URL);
                setProducts(data);
            } catch (err) {
                console.log(err);
            }
        };

        getProducts();
    }, []);

    useEffect(() => {
        localStorage.setItem(
            "addedToCartProducts",
            JSON.stringify(addedToCartProducts)
        );
    }, [addedToCartProducts]);


    useEffect(() => {
        localStorage.setItem("favoriteProducts", JSON.stringify(favoriteProducts));
    }, [favoriteProducts]);


    const handleAddToFavourite = (id) => {
        const productToAdd = products.find((product) => product.id === id);

        const isAlreadyFavourite = favoriteProducts.some(
            (product) => product.id === id
        );

        const updatedFavorites = isAlreadyFavourite
            ? favoriteProducts.filter((favProduct) => favProduct.id !== id)
            : [...favoriteProducts, productToAdd];

        setFavoriteProducts(updatedFavorites);

        const updatedProducts = products.map((product) =>
            product.id === id
                ? { ...product, isFavourite: !isAlreadyFavourite }
                : product
        );

        setProducts(updatedProducts);
    };

    const openFirstModalHandler = () => {
        setIsOpenFirstModal(!isOpenFirstModal);
    };

    const openSecondModalHandler = (id) => {
        setIsOpenSecondModal(true);

        setProductId(id);

        setProducts((products) => {
            const newProducts = structuredClone(products);
            const product = newProducts.find((el) => id === el.id);
            setChosenProduct(product);
            setProducts(newProducts);
        });
    };

    const handleCloseModal = () => {
        setIsOpenSecondModal(false);
        setProductId("");
    };

    const handleAddToCart = (id) => {

        let count = 0;

        const updatedProducts = products.map((product) =>
            product.id === productId ? { ...product, isAddedToCart: true } : product
        );

        addedToCartProducts.find(el => {
            //  console.log(el.id === chosenProduct.id)
            if (!(el.id === chosenProduct.id)) {

                setAddedToCartProducts([...addedToCartProducts]);
            } else {
                return count++;
            }
        })

        setProducts(updatedProducts);
        setAddedToCartProducts([...addedToCartProducts, chosenProduct]);
        setProductId("");
        setChosenProduct({});
    };

    const countFavouriteProducts = () => {
        return favoriteProducts.length;
    };

    const countAddedToCartProducts = () => {
        return addedToCartProducts.length;
    };

    const handleDeleteItem = (id) => {
        const updatedCartProducts = addedToCartProducts.filter(product => product.id !== id);
        setAddedToCartProducts(updatedCartProducts);
        console.log(updatedCartProducts);
    };



    return (
        <>
            <Header
                countFavouriteProducts={countFavouriteProducts}
                countAddedToCartProducts={countAddedToCartProducts}
            />
            {pathname !== "/cart" && pathname !== "/favourites" && (
                <ProductsContainer
                    products={products}
                    handleAddToFavourite={handleAddToFavourite}
                    openSecondModalHandler={openSecondModalHandler}
                    favoriteProducts={favoriteProducts}
                />
            )}

        
            {isOpenSecondModal && (
                <ModalWrapper
                    text="Do you want to add this product to cart?"
                    bodyText={products.find((el) => {
                        return el.id === isOpenSecondModal
                    }).name}
                    className="modal"
                    hasImage="false"
                    footerCancelBtn="false"
                    footerDeleteBtn="false"
                    footerAddToFavBtn="true"
                    thirdText="ADD TO CART"
                    modalIsOpen={() => openSecondModalHandler(null)}
                    handleAddToFavourite={() => { addToCart(); openSecondModalHandler(null) }}
                    products={products}
                />
            )}

{isOpenSecondModal && (
                <ModalWrapper
                    text={`Do you want to add to cart ${chosenProduct.name} ?`}
                    bodyText={`Color: ${chosenProduct.color}`}
                    className="modal"
                    hasImage="true"
                    imagePath={chosenProduct.imagePath}
                    footerCancelBtn="false"
                    footerDeleteBtn="false"
                    footerAddToCart="true"
                    thirdText="YES, ADD TO CART"
                    modalIsOpen={openSecondModalHandler}
                    handleAddToCart={handleAddToCart}
                    countAddedToCartProducts={countAddedToCartProducts}
                    handleCloseModal={handleCloseModal}
                    productId={productId}
                />
            )}

            <AppRouter
                handleAddToFavourite={handleAddToFavourite}
                openSecondModalHandler={openSecondModalHandler}
                favoriteProducts={favoriteProducts}
                produducts={products}
                addedToCartProducts={addedToCartProducts}
                handleDeleteItem={handleDeleteItem} 
            />

        </>
    );
}

export default App;
