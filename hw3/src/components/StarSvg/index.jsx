
import styles from "./StarSvg.module.scss";
import PropTypes from 'prop-types';
import classNames from "classnames";


const StarSvg = ({
    placedOnCard = false,
    id = "",
    isFavourite = false,
    handleAddToFavourite = () => { } }) => {
    return (
        <>
            <svg
                onClick={() => { handleAddToFavourite(id) }}
                className={`
                ${styles.starSvgBasic} 
                ${placedOnCard ? styles.starSvg : ""}
                ${isFavourite ? styles.active : ''}`}
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24" width={25} height={25}
            >
                <path  fill="#fff" d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z" />
            </svg>

        </>
    );
};

StarSvg.propTypes = {
    placedOnCard: PropTypes.bool,
    handleAddToFavourite: PropTypes.func,
    id: PropTypes.number,
    isFavourite: PropTypes.bool,
};


export default StarSvg;
