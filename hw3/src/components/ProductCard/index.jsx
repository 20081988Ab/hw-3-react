

import PropTypes from 'prop-types';
import styles from './ProductCard.module.scss';
import StarSvg from '../StarSvg';
import Button from '../Button';


const ProductCard = ({
  id,
  name = "",
  image = "",
  price,
  // imagePath = "",
 
  handleAddToFavourite = () => { },
  isFavourite = false,
  openSecondModalHandler,
  


}) => {


  return (
    <div className={styles.productCard}>
      {/* <div
        className={styles.image}
        style={{ backgroundImage: `url(${imagePath})` }}
      ></div> */}

      <div className={styles.productInfo}>
        <img src={image} alt={name} />
        <h2 className={styles.productName}>{name}</h2>
        <p className={styles.productPrice}>Price: {price}</p>
      </div>

      <StarSvg
        placedOnCard={true}
        isFavourite={isFavourite}
        id={id}
        handleAddToFavourite={handleAddToFavourite}
      />

      <Button onClick={() => {
        openSecondModalHandler(id)
      }} className={styles.addToCartBtn}>Add To Cart</Button>

    </div>
  );
};

ProductCard.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  imagePath: PropTypes.string.isRequired,
  handleAddToFavourite: PropTypes.func.isRequired,
  isFavourite: PropTypes.bool,
  openSecondModalHandler: PropTypes.func.isRequired,
};

export default ProductCard;
