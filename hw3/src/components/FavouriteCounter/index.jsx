import StarSvg from "../StarSvg";
import styles from "./FavouriteCounter.module.scss";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import classnames from "classnames";


const FavouriteCounter = ({ countFavouriteProducts }) => {
  return (
    <NavLink
      to="/favourites"
      className={({ isActive }) =>
        classnames(styles.FavouriteCounterContainer, {
          [styles.active]: isActive,
        })
      }
    >
      <StarSvg
        headerStar="true"
        countFavouriteProducts={countFavouriteProducts}
      />
      <p className={styles.counter}>{countFavouriteProducts()}</p>
    </NavLink>
  );
};

FavouriteCounter.propTypes = {
  countFavouriteProducts: PropTypes.func.isRequired,
};

export default FavouriteCounter;
