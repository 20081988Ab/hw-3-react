
import styles from './Header.module.scss';
import PropTypes from 'prop-types';
import CartSvg from '../CartSvg';
import FavouriteCounter from "../FavouriteCounter"
import HomeSvg from "../HomeSvg"

const Header = ({ countFavouriteProducts, countAddedToCartProducts }) => {

    return (
        <header className={styles.header}>
            <div className={styles.container}>
                <a className={styles.shopName} href="#">
                    Parfume shop
                </a>

                <nav className={styles.cartContainer}>
                    <HomeSvg />
                    <FavouriteCounter countFavouriteProducts={countFavouriteProducts} />
                    <CartSvg countAddedToCartProducts={countAddedToCartProducts} />
                </nav>
            </div>
        </header>
    );
};

Header.propTypes = {
    countFavouriteProducts: PropTypes.func.isRequired,
    countAddedToCartProducts: PropTypes.func.isRequired,
};

export default Header;

