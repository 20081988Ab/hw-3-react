import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';
import styles from './ProductsContainer.module.scss';


const ProductsContainer = ({
  products = [],
  handleAddToFavourite = () => { },
  openSecondModalHandler,
  favoriteProducts,
}) => {

  return (
    <div className={styles.productsContainer}>
      {products.map(({ id, name, image, price }) => {
        const isFavourite = favoriteProducts.some(
          (favProduct) => favProduct.id === id);

        return (

          <ProductCard
            id={id}
            key={`${id}-${Math.floor(Math.random() * 90) + 1000}`}
            name={name}
            image={image}
            price={price}
            isFavourite={isFavourite}
            handleAddToFavourite={handleAddToFavourite}
            openSecondModalHandler={openSecondModalHandler}
          />
        );
      })}
    </div>
  );
};

ProductsContainer.propTypes = {
  products: PropTypes.array,
  handleAddToFavourite: PropTypes.func,
  openSecondModalHandler: PropTypes.func.isRequired,
  favoriteProducts: PropTypes.array.isRequired,
};

export default ProductsContainer;
