import PropTypes from 'prop-types'

import Modal from "..";

const ModalWrapper = ({
  text,
  bodyText,
  className,
  hasImage,
  imagePath,
  footerCancelBtn,
  firstText,
  footerDeleteBtn,
  secondText,
  footerAddToCart,
  thirdText,
  modalIsOpen,
  handleAddToCart,
  handleCloseModal,
  countAddedToCartProducts,
  productId
}) => {

  
  return (
    <div className="modalWrapper">
      <Modal
        text={text}
        bodyText={bodyText}
        className={className}
        hasImage={hasImage}
        imagePath={imagePath}
        footerCancelBtn={footerCancelBtn}
        firstText={firstText}
        footerDeleteBtn={footerDeleteBtn}
        secondText={secondText}
        footerAddToCart={footerAddToCart}
        thirdText={thirdText}
        modalIsOpen={modalIsOpen}
        handleAddToCart={handleAddToCart}
        handleCloseModal={handleCloseModal}
        countAddedToCartProducts={countAddedToCartProducts}
        productId={productId}
      />
    </div>
  );
};

ModalWrapper.propTypes = {
  text: PropTypes.string.isRequired,
  bodyText: PropTypes.string.isRequired,
  className: PropTypes.string,
  hasImage: PropTypes.oneOf(['true', 'false']).isRequired,
  imagePath: PropTypes.string.isRequired,
  footerCancelBtn: PropTypes.string,
  firstText: PropTypes.string,
  footerDeleteBtn: PropTypes.string,
  secondText: PropTypes.string,
  footerAddToCart: PropTypes.oneOf(['true', 'false']).isRequired,
  footerAddToFavBtn: PropTypes.string,
  thirdText: PropTypes.string,
  modalIsOpen: PropTypes.func.isRequired,
  handleAddToCart: PropTypes.func.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
  countAddedToCartProducts: PropTypes.func.isRequired,
  productId: PropTypes.number.isRequired,
};

export default ModalWrapper;
