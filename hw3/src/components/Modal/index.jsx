import PropTypes from "prop-types";
import ModalImage from "./ModalImage";
import ModalHeader from "./ModalHeader";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import ModalBackground from "./ModalBackground";

const Modal = ({
  text,
  className,
  bodyText,
  hasImage,
  imagePath,
  footerCancelBtn,
  firstText,
  footerDeleteBtn,
  secondText,
  footerAddToCart,
  thirdText,
  modalIsOpen,
  handleAddToCart,
  handleCloseModal,
  countAddedToCartProducts,
  productId
  
}) => {
  
  return (
    <>
      <div className={className}>
        {hasImage === "true" && <ModalImage imagePath={imagePath}/>}
        <ModalHeader>{text}</ModalHeader>
        <ModalClose handleCloseModal={handleCloseModal} />
        <ModalBody bodyText={bodyText} />
        <ModalFooter
          footerCancelBtn={footerCancelBtn}
          firstText={firstText}
          footerDeleteBtn={footerDeleteBtn}
          secondText={secondText}
          footerAddToCart={footerAddToCart}
          thirdText={thirdText}
          modalIsOpen={modalIsOpen}
          firstClick={() => {}}
          secondaryClick={() => {}}
          handleAddToCart={handleAddToCart}
          handleCloseModal={handleCloseModal}
          countAddedToCartProducts={countAddedToCartProducts}
          productId={productId}
        />
      </div>

      <ModalBackground handleCloseModal={handleCloseModal} />
    </>
  );
};

Modal.propTypes = {
  text: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  bodyText: PropTypes.string.isRequired,
  hasImage: PropTypes.oneOf(["true", "false"]).isRequired,
  imagePath: PropTypes.string.isRequired,
  footerCancelBtn: PropTypes.string,
  firstText: PropTypes.string,
  footerDeleteBtn: PropTypes.string,
  secondText: PropTypes.string,
  footerAddToCart: PropTypes.oneOf(["true", "false"]).isRequired,
  footerAddToFavBtn: PropTypes.string,
  thirdText: PropTypes.string,
  modalIsOpen: PropTypes.func.isRequired,
  handleAddToCart: PropTypes.func.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
  countAddedToCartProducts: PropTypes.func.isRequired,
  productId: PropTypes.number.isRequired,
};

export default Modal;
