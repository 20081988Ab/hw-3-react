// const src = "/productImages/1.jpg";
// const ModalImage = () => {
    
//     return (<div className="modalImage">
//         {<img src={src} alt="product" />}
//     </div>)
// }

// export default ModalImage;
import styles from "./ModalImage.module.scss"
import PropTypes from 'prop-types'

const ModalImage = ({imagePath}) => {

   return (
       <img className={styles.image} src={imagePath} alt="" ></img>
   )
}

ModalImage.propTypes = {
   imagePath: PropTypes.string,
 };

export default ModalImage;
