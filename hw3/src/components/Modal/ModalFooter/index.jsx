import Button from "../../Button";
import PropTypes from "prop-types";
import styles from "./ModalFooter.module.scss"
import { useState } from "react";

const ModalFooter = ({
  footerCancelBtn,
  firstText,
  footerDeleteBtn,
  secondText,
  footerAddToCart,
  thirdText,
  modalIsOpen,
  firstClick,
  secondaryClick,
  handleAddToCart,
  handleCloseModal,
  countAddedToCartProducts,
  productId
}) => {


  return (
    <div className="footer">
      {footerCancelBtn === "true" ? (
        <Button 
        onClick={modalIsOpen} 
        className={styles.modalBtn}>
          {" "}
          {firstText}
        </Button>
      ) : (
        ""
      )}
      {footerDeleteBtn === "true" ? (
        <Button className="transparentBtn"> {secondText}</Button>
      ) : (
        ""
      )}
      {footerAddToCart === "true" ? (
        <Button
          onClick={() => {
            handleCloseModal();
            handleAddToCart(productId);
            countAddedToCartProducts();
          }}
          className={styles.modalBtn}
        >
          {" "}
          {thirdText}
        </Button>
      ) : (
        ""
      )}
    </div>
  );
};

ModalFooter.propTypes = {
  footerCancelBtn: PropTypes.oneOf(["true", "false"]).isRequired,
  firstText: PropTypes.string,
  footerDeleteBtn: PropTypes.oneOf(["true", "false"]).isRequired,
  secondText: PropTypes.string,
  footerAddToCart: PropTypes.oneOf(["true", "false"]).isRequired,
  thirdText: PropTypes.string.isRequired,
  modalIsOpen: PropTypes.func.isRequired,
  firstClick: PropTypes.func.isRequired,
  secondaryClick: PropTypes.func.isRequired,
  handleAddToCart: PropTypes.func.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
  countAddedToCartProducts: PropTypes.func.isRequired,
  productId: PropTypes.number.isRequired,
};

export default ModalFooter;

