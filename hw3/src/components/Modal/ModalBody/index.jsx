import PropTypes from "prop-types";

const ModalBody = ({ bodyText = 'Product description' }) => {
  return (
    <div className="modalBody">


      <p className="bodyText">{bodyText}</p>


    </div>
  );
};

ModalBody.propTypes = {
  bodyText: PropTypes.string,
};

export default ModalBody;

