import React from 'react';
import PropTypes from 'prop-types';
import styles from './CartItem.module.scss';

const CartItem = ({ product, handleDeleteItem = { handleDeleteItem } }) => {
    const { id, imagePath, name, price, } = product;

    const handleDeleteButtonClick = () => {

        handleDeleteItem(id);
    };

    return (
        <div className={styles.itemContainer}>
            <img src={imagePath} className={styles.image} alt="" />
            <div className={styles.addInfo}>

                <span> {name}</span>

            </div>
            <div className={styles.price}>
                <span>Total: 1</span>
                <span>Price: {price}</span>
            </div>
            <button className={styles.deleteBtn} onClick={handleDeleteButtonClick}>Delete</button>
        </div>
    );
};

CartItem.propTypes = {
    product: PropTypes.object.isRequired,
    handleDeleteItem: PropTypes.func,
};

export default CartItem;
